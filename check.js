
/*
*  Code to check whether this sudoku solution is valid of not
*/

var validSoluion1 = [
  [5,8,1, 6,7,2, 4,3,9],
  [7,9,2, 8,4,3, 6,5,1],
  [3,6,4, 5,9,1, 7,8,2],

  [4,3,8, 9,5,7, 2,1,6],
  [2,5,6, 1,8,4, 9,7,3],
  [1,7,9, 3,2,6, 8,4,5],

  [8,4,5, 2,1,9, 3,6,7],
  [9,1,3, 7,6,8, 5,2,4],
  [6,2,7, 4,3,5, 1,9,8]
];
var wrongSolution1 = [
  [5,8,1, 6,7,2, 4,3,9],
  [7,9,2, 8,4,3, 6,5,1],
  [3,6,4, 5,9,1, 7,8,2],

  [4,3,8, 9,5,7, 2,1,6],
  [2,5,6, 1,8,4, 9,7,3],
  [1,7,9, 3,2,6, 8,4,5],

  [8,4,5, 2,1,9, 3,6,7],
  [9,3,3, 7,6,8, 5,2,4],
  [6,2,7, 4,3,5, 1,9,8]
];

var validSoluion2 = [
  [7,8,4, 1,5,9, 3,2,6],
  [5,3,9, 6,7,2, 8,4,1],
  [6,1,2, 4,3,8, 7,5,9],

  [9,2,8, 7,1,5, 4,6,3],
  [3,5,7, 8,4,6, 1,9,2],
  [4,6,1, 9,2,3, 5,8,7],

  [8,7,6, 3,9,4, 2,1,5],
  [2,4,3, 5,6,1, 9,7,8],
  [1,9,5, 2,8,7, 6,3,4]
];
var wrongSolution2 = [
  [7,8,4, 1,5,9, 3,2,6],
  [5,3,9, 6,7,2, 8,4,1],
  [6,1,2, 4,3,8, 7,5,9],

  [9,2,8, 7,1,5, 4,6,3],
  [3,5,7, 8,4,6, 1,9,2],
  [4,6,1, 9,2,3, 5,8,7],

  [8,7,6, 3,9,4, 2,1,5],
  [2,4,3, 5,6,1, 9,7,8],
  [1,9,5, 2,3,7, 6,5,4]
];

var inputSudokus = [validSoluion1,wrongSolution1,validSoluion2,wrongSolution2];

for( let i =0; i<inputSudokus.length ;i++){
	console.log(checkSudoku(inputSudokus[i]));
}

function checkInputValidation(){
	// let assume that suduko is in a valid format
	return true;
}
/*
* Rules to check
* 1. all elemts 1-9 should be present in earch row
* 2. all elemts 1-9 should be present in each coloumn
* 3. all elemts 1-9 should be present in each mini box (3*3)
*/
function checkSudoku(sudoku) {
    // check size validation for sudoku
    checkInputValidation();

    // verifiy horizontal elements
    // all elemts 1-9 should be present in earch row
    var validSudoku = true;
    var seeError;
    for (let i = 0; i < sudoku.length; i++) {
    	let temp = sudoku[i];
    	validSudoku = validSudoku && temp.every(function(value, index) {
    		 return temp.indexOf(index + 1) > -1; 
    		});
    	if(validSudoku === false){
    		seeError = temp;
    		break;
    	}
    };

    if(validSudoku === false){
    	return 'Error during row element testing in '+ seeError;
    }

    // verifiy vertical elements
	// all elemts 1-9 should be present in each coloumn
    for (let i = 0; i < sudoku.length; i++) {
    	//get the vertical elemets in a seperate array
        let temp  = sudoku.map(function(val) {
	         return val[i]; 
	     });
        validSudoku = validSudoku && sudoku[i].every(function(val, i) { return temp.indexOf(i + 1) > -1; });
        if(validSudoku === false){
    		seeError = temp;
    		break;
    	}
    }

    if(validSudoku === false){
    	return 'Error during column element testing in '+seeError;
    }

    // verifiy each minibox

    // size of mini box
    let boxsize = Math.sqrt(sudoku.length);
    let temparr = [];
    for (var i = 0; i < boxsize; i++) {

        sudoku.forEach(function(val, e) {
            side  = val.slice(boxsize * i, boxsize * i + boxsize);
            temparr  = temparr.concat(side);
            if ((e+1) % boxsize == 0 && e != 0) {
                for (var j = 1; j <= sudoku.length; j++){
                    if (temparr.indexOf(j) < 0){
                    	validSudoku = false; 
                   	}
                }      
                // reset array after checking current box          
                temparr = [];
            }
        });

    }
    if(validSudoku === false){
    	return 'Error during 3*3 box element testing';
    }
    return validSudoku;
}